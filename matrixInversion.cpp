#include "benchmark/benchmark.h"
#include "Eigen/Eigen"
#include <iostream>

// same code as from Stewart: https://gist.github.com/StewMH/c425aaaaf84a434c2dff590aa734e658

Eigen::MatrixXd GetRandomSymMatrix(int dim) {
  Eigen::MatrixXd x = Eigen::MatrixXd::Random(dim,dim);
  for (int i = 0; i != dim; ++i) {
      for (int j = 0; j <= i; ++j) {
        x(i,j) = x(j,i);
      }
  }
  return x*x.transpose();
}

int N_RowsCols = 10;
Eigen::MatrixXd xg = GetRandomSymMatrix(N_RowsCols);

static void BM_Eigen_Matrix_inv(benchmark::State& state) {
  Eigen::MatrixXd x = xg;
  while (state.KeepRunning()) {
    Eigen::MatrixXd x_inv = x.inverse();
    benchmark::DoNotOptimize(x_inv);
  }
}

static void BM_Eigen_SelfAdjoint_Matrix_inv_Upper(benchmark::State& state) {
  Eigen::MatrixXd x = xg;
  Eigen::MatrixXd x_SA = x.selfadjointView<Eigen::Upper>();
  while (state.KeepRunning()) {
    Eigen::MatrixXd x_inv = x_SA.inverse();
    benchmark::DoNotOptimize(x_inv);
  }
}

static void BM_Eigen_SelfAdjoint_Matrix_inv_llt_solve_Upper(benchmark::State& state) {
  Eigen::MatrixXd x = xg;
  while (state.KeepRunning()) {
    Eigen::MatrixXd x_inv = x.selfadjointView<Eigen::Upper>().llt().solve(Eigen::MatrixXd::Identity(N_RowsCols, N_RowsCols));
    benchmark::DoNotOptimize(x_inv);
  }
}

//static void BM_Eigen_SelfAdjoint_Matrix_inv_llt_solve_Upper(benchmark::State& state) {
//  Eigen::MatrixXd x = xg;
//  Eigen::MatrixXd x_SA = x.selfadjointView<Eigen::Upper>();
//  while (state.KeepRunning()) {
    //Eigen::MatrixXd x_inv = x_SA.llt().solve(Eigen::MatrixXd::Identity(N_RowsCols, N_RowsCols));
//    Eigen::MatrixXd x_inv = x.selfadjointView<Eigen::Upper>().llt().solve(Eigen::MatrixXd::Identity(N_RowsCols, N_RowsCols));
//    benchmark::DoNotOptimize(x_inv);
//  }
//}

static void BM_Eigen_Matrix_inv_llt_solve_Upper(benchmark::State& state) {
  Eigen::MatrixXd x = xg;
  while (state.KeepRunning()) {
    Eigen::MatrixXd x_inv = x.llt().solve(Eigen::MatrixXd::Identity(N_RowsCols, N_RowsCols));
    benchmark::DoNotOptimize(x_inv);
  }
}

static void BM_Eigen_SelfAdjoint_Matrix_Upper(benchmark::State& state) {
  Eigen::MatrixXd x = xg;
  while (state.KeepRunning()) {
    Eigen::MatrixXd x_SA = x.selfadjointView<Eigen::Upper>();
    benchmark::DoNotOptimize(x_SA);
  }
}

static void BM_Eigen_SelfAdjoint_Matrix_inv_Lower(benchmark::State& state) {
  Eigen::MatrixXd x = xg;
  while (state.KeepRunning()) {
    Eigen::MatrixXd x_SA = x.selfadjointView<Eigen::Lower>();
    Eigen::MatrixXd x_inv = x_SA.inverse();
    benchmark::DoNotOptimize(x_inv);
  }
}

BENCHMARK(BM_Eigen_Matrix_inv);
BENCHMARK(BM_Eigen_SelfAdjoint_Matrix_inv_Upper);
BENCHMARK(BM_Eigen_Matrix_inv_llt_solve_Upper);
BENCHMARK(BM_Eigen_SelfAdjoint_Matrix_inv_llt_solve_Upper);
//BENCHMARK(BM_Eigen_SelfAdjoint_Matrix_Upper);
//BENCHMARK(BM_Eigen_SelfAdjoint_Matrix_inv_Lower);
BENCHMARK_MAIN();

//Compile with gcc 6.2
// g++ -O2 -lrt -o erf -std=c++14 ../src/erf.cpp -I ../benchmark/include/ -lbenchmark -L src/ `root-config --cflags --ldflags --libs` -I /cvmfs/atlas-nightlies.cern.ch/repo/sw/21.1/sw/lcg/releases/LCG_87/eigen/3.2.9/x86_64-slc6-gcc62-opt/include/eigen3
